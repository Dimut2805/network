package ru.uds.transfermessage;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        try (ServerSocket server = new ServerSocket(2805)) {
            Socket client = server.accept();
            DataInputStream in = new DataInputStream(client.getInputStream());
            DataOutputStream out = new DataOutputStream(client.getOutputStream());
            String string = in.readUTF();
            System.out.println("Сообщение: " + string);
            out.writeUTF(string);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
